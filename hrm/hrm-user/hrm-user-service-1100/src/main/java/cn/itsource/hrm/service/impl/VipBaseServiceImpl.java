package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipBase;
import cn.itsource.hrm.mapper.VipBaseMapper;
import cn.itsource.hrm.service.IVipBaseService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员基本信息 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipBaseServiceImpl extends ServiceImpl<VipBaseMapper, VipBase> implements IVipBaseService {

}
