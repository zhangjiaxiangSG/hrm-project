package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipLoginLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 登录记录 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipLoginLogService extends IService<VipLoginLog> {

}
