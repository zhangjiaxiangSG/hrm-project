package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipUser;
import cn.itsource.hrm.dto.UserRegisterDto;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员登录账号 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipUserService extends IService<VipUser> {
    /**
     * 注册
     * @param registerDto
     */
    void register(UserRegisterDto registerDto);
}
