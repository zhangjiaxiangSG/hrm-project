package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipMsg;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 站内信 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipMsgService extends IService<VipMsg> {

}
