package cn.itsource.hrm.service;

import cn.itsource.hrm.dto.SendSMSDto;

public interface IVerifyCodeService {
    /**
     * 创建验证码
     * @param key
     * @return
     */
    String createImageCode(String key);

    /**
     * 发送短信验证码
     * @param sendSMSCode
     */
    void sendSMSCode(SendSMSDto sendSMSCode);
}
