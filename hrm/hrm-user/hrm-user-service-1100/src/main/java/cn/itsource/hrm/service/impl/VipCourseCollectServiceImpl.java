package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipCourseCollect;
import cn.itsource.hrm.mapper.VipCourseCollectMapper;
import cn.itsource.hrm.service.IVipCourseCollectService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品收藏 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipCourseCollectServiceImpl extends ServiceImpl<VipCourseCollectMapper, VipCourseCollect> implements IVipCourseCollectService {

}
