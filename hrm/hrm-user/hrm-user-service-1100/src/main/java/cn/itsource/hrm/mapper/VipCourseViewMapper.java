package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipCourseView;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品浏览 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipCourseViewMapper extends BaseMapper<VipCourseView> {

}
