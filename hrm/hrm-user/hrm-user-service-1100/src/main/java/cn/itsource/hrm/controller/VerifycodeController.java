package cn.itsource.hrm.controller;

import cn.itsource.hrm.dto.SendSMSDto;
import cn.itsource.hrm.service.IVerifyCodeService;
import cn.itsource.hrm.util.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

//验证码相关
@RestController
public class VerifycodeController {

    @Autowired
    private IVerifyCodeService verifyCodeService;

    //图片验证码
    @RequestMapping(value = "/verifycode/imageCode/{key}",method = RequestMethod.GET)
    public AjaxResult createImageCode(@PathVariable("key") String key){
        String base64ImgStr = verifyCodeService.createImageCode(key);
        return AjaxResult.me().setResultObj(base64ImgStr) ;
    }
    //发送短信验证码
    @RequestMapping(value = "/verifycode/sendSmsCode",method = RequestMethod.POST)
    public AjaxResult sendSMSCode(@RequestBody SendSMSDto sendSMSCode){
        verifyCodeService.sendSMSCode(sendSMSCode);
        return AjaxResult.me();
    }
}
