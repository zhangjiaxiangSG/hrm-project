package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipMsg;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 站内信 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipMsgMapper extends BaseMapper<VipMsg> {

}
