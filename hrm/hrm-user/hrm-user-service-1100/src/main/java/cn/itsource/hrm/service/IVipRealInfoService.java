package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipRealInfo;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员实名资料 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipRealInfoService extends IService<VipRealInfo> {

}
