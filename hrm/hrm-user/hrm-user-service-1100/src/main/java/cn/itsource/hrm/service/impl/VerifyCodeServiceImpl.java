package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.dto.SendSMSDto;
import cn.itsource.hrm.service.IVerifyCodeService;
import cn.itsource.hrm.to.RedisSMSVerifyTo;
import cn.itsource.hrm.util.AssertUtil;
import cn.itsource.hrm.util.StrUtils;
import cn.itsource.hrm.util.VerifyCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

import static cn.itsource.hrm.util.RedisConstant.*;

@Service
@Slf4j
public class VerifyCodeServiceImpl implements IVerifyCodeService {
    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;

    /**
     *获取图片验证码数据
     * @param key
     * @return
     */
    @Override
    public String createImageCode(String key) {
        //检验数据
        AssertUtil.isNotNull(key, "验证码生成失败！");
        //获取图片的验证码
        String code = UUID.randomUUID().toString().substring(0, 4);
        //验证码存储在redis里面
        redisTemplate.opsForValue().set(key, code,600, TimeUnit.SECONDS);
        //验证码转换成图片
        //图片转换成base64字符串
        String baseurl = VerifyCodeUtils.VerifyCode(140, 40, code);
        //返回字符串
        return baseurl;
    }

    /**
     * 发送短信验证码
     * @param sendSMSCode
     */
    @Override
    public void sendSMSCode(SendSMSDto sendSMSCode) {
        //校验数据
        String imageCode = sendSMSCode.getImageCode();
        String imageCodeKey = sendSMSCode.getImageCodeKey();
        String mobile = sendSMSCode.getMobile();
        AssertUtil.isNotNull(imageCode, "短信验证码为空！");
        AssertUtil.isNotNull(imageCodeKey, "短信验证为空，请刷新页面后重试！");
        AssertUtil.isNotNull(mobile, "手机号不能为空，请输入手机号！");
        //查看当前图片的验证码是否一致
        String redisImageCodeKey = (String)redisTemplate.opsForValue().get(imageCodeKey);
        AssertUtil.isEqualsIgnoreCase(redisImageCodeKey,imageCode,"图片验证错误，请重新输入！");
        //查看当前手机号的是否存在redis里面
        String format = String.format(SEND_SMS_CODE_FORMAT, mobile);
        RedisSMSVerifyTo  PhoneverifyCode = (RedisSMSVerifyTo)redisTemplate.opsForValue().get(format);
        //生成一个手机验证码
        String randomString = null;
        //如果存在
        if(!StringUtils.isEmpty(PhoneverifyCode)){
            //存在的话判断在redis中是否大于60秒
            boolean b = (new Date().getTime() - PhoneverifyCode.getDateTime()) >= 60 * 1000;
            //小于60秒抛出重复异常
            AssertUtil.isTrue(b, "发送频繁，稍后再试！");
            //大于60秒则重新发送验证码
            randomString = PhoneverifyCode.getPhoneVerifyCode();
        }else{
            //如果不存在
            //生成验证码
            randomString = StrUtils.getRandomString(4);
        }
        String phoneFormat = String.format(SEND_SMS_CODE_FORMAT, mobile);
        //设置reids存储的value值，封装成对象
        RedisSMSVerifyTo redisSMSVerifyTo = new RedisSMSVerifyTo(randomString,new Date().getTime());
        //存储在redis里面SMS:手机号,值为验证嘛的值,有效期十分钟
        redisTemplate.opsForValue().set(phoneFormat,redisSMSVerifyTo,10, TimeUnit.MINUTES);
        //发送信息
        log.info("尊敬的用户，验证码为："+randomString);
        //保存日志

    }

    private void sendSMSCode(String mobile) {
        //生成一个手机验证码
        String randomString = StrUtils.getRandomString(4);
        String phoneFormat = String.format(SEND_SMS_CODE_FORMAT, mobile);
        //设置reids存储的value值，封装成对象
        RedisSMSVerifyTo redisSMSVerifyTo = new RedisSMSVerifyTo(randomString,new Date().getTime());
        //存储在redis里面SMS:手机号,值为验证嘛的值,有效期十分钟
        redisTemplate.opsForValue().set(phoneFormat,redisSMSVerifyTo,10, TimeUnit.MINUTES);
        //发送信息
        log.info("尊敬的用户，验证码为："+randomString);
        //保存日志
    }
}
