package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipGrowLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 成长值记录 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipGrowLogMapper extends BaseMapper<VipGrowLog> {

}
