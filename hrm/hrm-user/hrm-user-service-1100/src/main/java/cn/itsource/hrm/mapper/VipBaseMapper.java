package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipBase;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员基本信息 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipBaseMapper extends BaseMapper<VipBase> {

}
