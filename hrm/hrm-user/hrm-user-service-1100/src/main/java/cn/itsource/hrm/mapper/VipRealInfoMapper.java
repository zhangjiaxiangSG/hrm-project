package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipRealInfo;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员实名资料 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipRealInfoMapper extends BaseMapper<VipRealInfo> {

}
