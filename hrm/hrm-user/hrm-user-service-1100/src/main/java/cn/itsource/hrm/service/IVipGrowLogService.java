package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipGrowLog;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 成长值记录 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipGrowLogService extends IService<VipGrowLog> {

}
