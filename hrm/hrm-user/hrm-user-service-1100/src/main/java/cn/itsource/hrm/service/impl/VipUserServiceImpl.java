package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipBase;
import cn.itsource.hrm.domain.VipUser;
import cn.itsource.hrm.dto.UserRegisterDto;
import cn.itsource.hrm.mapper.VipBaseMapper;
import cn.itsource.hrm.mapper.VipUserMapper;
import cn.itsource.hrm.service.IVipUserService;
import cn.itsource.hrm.to.RedisSMSVerifyTo;
import cn.itsource.hrm.util.AssertUtil;
import cn.itsource.hrm.util.RedisConstant;
import cn.itsource.hrm.util.encrypt.MD5;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.UUID;

import static cn.itsource.hrm.domain.VipUser.BITSTATE_PHONE;

/**
 * <p>
 * 会员登录账号 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipUserServiceImpl extends ServiceImpl<VipUserMapper, VipUser> implements IVipUserService {
    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;
    @Autowired
    private VipBaseMapper vipBaseMapper;
    @Override
    public void register(UserRegisterDto registerDto) {
        //校验数据
        String mobile = registerDto.getMobile();
        String password = registerDto.getPassword();
        Integer regChannel = registerDto.getRegChannel();
        String smsCode = registerDto.getSmsCode();
        //非空验证
        AssertUtil.isNotNull(mobile, "手机号不正确！");
        AssertUtil.isNotNull(password, "密码不正确！");
        AssertUtil.isNotNull(regChannel, "请刷新页面重试！非法渠道");
        Boolean vipuserByPhone = baseMapper.selectByPhone(mobile);
        AssertUtil.isTrue(!vipuserByPhone, "手机号已注册！");
        //验证短信验证码是否正确
        RedisSMSVerifyTo redisSMSVerifyTo = (RedisSMSVerifyTo)redisTemplate.opsForValue().get(String.format(RedisConstant.SEND_SMS_CODE_FORMAT,mobile));
        AssertUtil.isNotNull(redisSMSVerifyTo, "手机验证码已过期");
        String redisSMSCode = redisSMSVerifyTo.getPhoneVerifyCode();
        AssertUtil.isEqualsIgnoreCase(smsCode, redisSMSCode, "手机验证码不正确！请重新输入");
        //数据保存在user表里面
        Date now = new Date();
        VipUser vipUser = new VipUser();
        vipUser.setPhone(mobile);
        String salt = UUID.randomUUID().toString();
        vipUser.setSalt(salt);
        vipUser.setPassword(MD5.getMD5(password+salt));
        vipUser.setCreateTime(now.getTime());
        vipUser.setBitState(vipUser.addBitState(BITSTATE_PHONE));
        baseMapper.insert(vipUser);
        //数据保存在base表里面
        VipBase vipBase = new VipBase();
        vipBase.setCreateTime(now.getTime());
        vipBase.setRegChannel(regChannel);
        vipBase.setRegTime(now.getTime());
        vipBase.setSsoId(vipUser.getId());
        vipBaseMapper.insert(vipBase);
    }
}
