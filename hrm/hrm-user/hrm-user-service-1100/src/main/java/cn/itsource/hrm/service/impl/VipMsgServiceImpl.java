package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipMsg;
import cn.itsource.hrm.mapper.VipMsgMapper;
import cn.itsource.hrm.service.IVipMsgService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 站内信 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipMsgServiceImpl extends ServiceImpl<VipMsgMapper, VipMsg> implements IVipMsgService {

}
