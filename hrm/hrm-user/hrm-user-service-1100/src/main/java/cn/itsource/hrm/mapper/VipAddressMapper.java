package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipAddress;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 收货地址 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipAddressMapper extends BaseMapper<VipAddress> {

}
