package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipRealInfo;
import cn.itsource.hrm.mapper.VipRealInfoMapper;
import cn.itsource.hrm.service.IVipRealInfoService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 会员实名资料 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipRealInfoServiceImpl extends ServiceImpl<VipRealInfoMapper, VipRealInfo> implements IVipRealInfoService {

}
