package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipCourseView;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品浏览 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipCourseViewService extends IService<VipCourseView> {

}
