package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipGrowLog;
import cn.itsource.hrm.mapper.VipGrowLogMapper;
import cn.itsource.hrm.service.IVipGrowLogService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 成长值记录 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipGrowLogServiceImpl extends ServiceImpl<VipGrowLogMapper, VipGrowLog> implements IVipGrowLogService {

}
