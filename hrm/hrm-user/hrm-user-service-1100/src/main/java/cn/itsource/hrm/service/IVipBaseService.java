package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipBase;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 会员基本信息 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipBaseService extends IService<VipBase> {

}
