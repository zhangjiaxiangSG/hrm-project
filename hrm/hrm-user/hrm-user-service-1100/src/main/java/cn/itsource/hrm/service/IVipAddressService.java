package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipAddress;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 收货地址 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipAddressService extends IService<VipAddress> {

}
