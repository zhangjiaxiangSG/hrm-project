package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipCourseCollect;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 商品收藏 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipCourseCollectMapper extends BaseMapper<VipCourseCollect> {

}
