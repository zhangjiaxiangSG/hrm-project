package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipLoginLog;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 登录记录 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipLoginLogMapper extends BaseMapper<VipLoginLog> {

}
