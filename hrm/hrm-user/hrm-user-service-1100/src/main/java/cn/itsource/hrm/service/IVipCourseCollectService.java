package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.VipCourseCollect;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 商品收藏 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface IVipCourseCollectService extends IService<VipCourseCollect> {

}
