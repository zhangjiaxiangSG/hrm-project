package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.VipUser;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 会员登录账号 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
public interface VipUserMapper extends BaseMapper<VipUser> {

    Boolean selectByPhone(String mobile);
}
