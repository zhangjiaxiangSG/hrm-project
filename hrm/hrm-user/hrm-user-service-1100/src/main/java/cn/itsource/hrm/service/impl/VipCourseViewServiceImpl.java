package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipCourseView;
import cn.itsource.hrm.mapper.VipCourseViewMapper;
import cn.itsource.hrm.service.IVipCourseViewService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 商品浏览 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipCourseViewServiceImpl extends ServiceImpl<VipCourseViewMapper, VipCourseView> implements IVipCourseViewService {

}
