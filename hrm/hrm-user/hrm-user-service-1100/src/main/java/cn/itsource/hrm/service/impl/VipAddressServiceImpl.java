package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.VipAddress;
import cn.itsource.hrm.mapper.VipAddressMapper;
import cn.itsource.hrm.service.IVipAddressService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 收货地址 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@Service
public class VipAddressServiceImpl extends ServiceImpl<VipAddressMapper, VipAddress> implements IVipAddressService {

}
