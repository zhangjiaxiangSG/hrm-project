package cn.itsource.hrm.dto;

import lombok.Data;

@Data
public class SendSMSDto {
    //图片验证码value
    private String imageCode;
    //图片验验证码键值
    private String imageCodeKey;
    //手机号
    private String mobile;
}
