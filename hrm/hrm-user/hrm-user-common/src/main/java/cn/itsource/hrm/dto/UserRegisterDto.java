package cn.itsource.hrm.dto;

import com.sun.istack.internal.NotNull;
import lombok.Data;

@Data
public class UserRegisterDto {
    private String imageCode;
    private String mobile;
    private String password;
    private Integer regChannel;
    private String smsCode;
}
