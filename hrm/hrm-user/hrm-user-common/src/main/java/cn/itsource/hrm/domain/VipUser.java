package cn.itsource.hrm.domain;

import com.baomidou.mybatisplus.annotations.TableField;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;

import java.io.Serializable;

/**
 * <p>
 * 会员登录账号
 * </p>
 *
 * @author zjx
 * @since 2021-01-20
 */
@TableName("t_vip_user")
public class VipUser extends com.baomidou.mybatisplus.activerecord.Model<VipUser> {
    //给定绑定手机的位状态  000001
    public static final long BITSTATE_PHONE = 1;
    //给定通过实名的位状态  000010
    public static final long BITSTATE_REAL_INFO = 2;
    //给定绑定邮箱的位状态  000100
    public static final long BITSTATE_EMAIL = 4;
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;
    @TableField("create_time")
    private Long createTime;
    @TableField("update_time")
    private Long updateTime;
    /**
     * 三方登录名
     */
    @TableField("third_uid")
    private String thirdUid;
    /**
     * 手机号
     */
    private String phone;
    /**
     * 邮箱
     */
    private String email;
    /**
     * 密码
     */
    private String password;
    /**
     * 头像
     */
    private String avatar;
    /**
     * 盐值
     */
    private String salt;
    /**
     * 昵称
     */
    @TableField("nick_name")
    private String nickName;
    /**
     * 用户状态
     */
    @TableField("bit_state")
    private Long bitState = 0l;
    /**
     * 安全级别
     */
    @TableField("sec_level")
    private Integer secLevel;
    @TableField("login_id")
    private Long loginId;
    //为状态加法
    public Long addBitState(Long other){
        return this.bitState | other;
    }
    //为状态减法
    public Long removeBitState(Long other){
        return this.bitState ^ other;
    }
    //给用户的位状态是否包含值
    public boolean containBitState(long state){
        return (this.bitState & state ) > 0;
    }
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getCreateTime() {
        return createTime;
    }

    public void setCreateTime(Long createTime) {
        this.createTime = createTime;
    }

    public Long getUpdateTime() {
        return updateTime;
    }

    public void setUpdateTime(Long updateTime) {
        this.updateTime = updateTime;
    }

    public String getThirdUid() {
        return thirdUid;
    }

    public void setThirdUid(String thirdUid) {
        this.thirdUid = thirdUid;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getNickName() {
        return nickName;
    }

    public void setNickName(String nickName) {
        this.nickName = nickName;
    }

    public Long getBitState() {
        return bitState;
    }

    public void setBitState(Long bitState) {
        this.bitState = bitState;
    }

    public Integer getSecLevel() {
        return secLevel;
    }

    public void setSecLevel(Integer secLevel) {
        this.secLevel = secLevel;
    }

    public Long getLoginId() {
        return loginId;
    }

    public void setLoginId(Long loginId) {
        this.loginId = loginId;
    }

    @Override
    protected Serializable pkVal() {
        return this.id;
    }

    @Override
    public String toString() {
        return "VipUser{" +
        ", id=" + id +
        ", createTime=" + createTime +
        ", updateTime=" + updateTime +
        ", thirdUid=" + thirdUid +
        ", phone=" + phone +
        ", email=" + email +
        ", password=" + password +
        ", avatar=" + avatar +
        ", salt=" + salt +
        ", nickName=" + nickName +
        ", bitState=" + bitState +
        ", secLevel=" + secLevel +
        ", loginId=" + loginId +
        "}";
    }
}
