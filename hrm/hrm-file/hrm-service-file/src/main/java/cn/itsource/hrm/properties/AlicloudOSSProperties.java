package cn.itsource.hrm.properties;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;

@Component
@ConfigurationProperties("file.alicloud")
@Data
public class AlicloudOSSProperties {
   private String bucketName;
   private String accessKey;
   private String secretKey;
   private String endpoint;
}
