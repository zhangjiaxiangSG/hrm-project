package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.mapper.HighlightResultMapper;
import cn.itsource.hrm.query.CourseQuery;
import cn.itsource.hrm.respository.CourseRespository;
import cn.itsource.hrm.service.ISearchService;
import cn.itsource.hrm.util.AggregationElement;
import cn.itsource.hrm.util.PageList;
import org.elasticsearch.index.query.BoolQueryBuilder;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.aggregations.Aggregation;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.aggregations.bucket.terms.StringTerms;
import org.elasticsearch.search.aggregations.bucket.terms.TermsAggregationBuilder;
import org.elasticsearch.search.fetch.subphase.highlight.HighlightBuilder;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.Query;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service
public class SearchServiceImpl implements ISearchService {
    @Autowired
    private CourseRespository courseRespository;
    @Autowired
    private HighlightResultMapper highlightResultMapper;
    @Override
    public void save(CourseDoc courseDoc) {
        courseRespository.save(courseDoc);
    }
    @Autowired
    ElasticsearchTemplate template;
    @Override
    public void delete(CourseDoc courseDoc) {
        courseRespository.delete(courseDoc);
    }

    /**
     * 课程的全文检索信息
     * @param courseQuery
     * @return
     */
    @Override
    public PageList<CourseDoc> CourseSearch(CourseQuery courseQuery) {
        //判断数据是否为空
        if(courseQuery == null){
            return new PageList<>(0,null);
        }
        //1.创建查询对象
        NativeSearchQueryBuilder Builder = new NativeSearchQueryBuilder();
        //2.分页
        Builder.withPageable(PageRequest.of(courseQuery.getPage()-1, courseQuery.getRows()));
        //3.设置排序方式,默认为降序
        SortOrder desc = SortOrder.DESC;
        if(!StringUtils.isEmpty(courseQuery.getSortType()) && courseQuery.getSortType().toLowerCase().equals("asc")){
            desc = SortOrder.ASC;
        }
        //设置他的排序字段,默认按销售量排序
        String fieldSort = "saleCount";
        if(!StringUtils.isEmpty(courseQuery.getSortField())){
            switch (courseQuery.getSortField().toLowerCase()){
                case "pl":fieldSort = "commentCount";break;
                case "jg":fieldSort = "price";break;
                case "rq":fieldSort = "viewCount";break;
            }
        }
        Builder.withSort(SortBuilders.fieldSort(fieldSort).order(desc));
        //4.设置组合查询条件
        //得到组合查询的查询对象
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //设置组合对象里面的must对象
        if(!StringUtils.isEmpty(courseQuery.getKeyword())){
            boolQuery.must(QueryBuilders.matchQuery("name", courseQuery.getKeyword()));
        }
        //设置组合对象里面的must对象
        if(!StringUtils.isEmpty(courseQuery.getGradeName())){
            boolQuery.must(QueryBuilders.termQuery("gradeName", courseQuery.getGradeName()));
        }
        //设置组合对象里面的must对象
        if(!StringUtils.isEmpty(courseQuery.getTenantName())){
            boolQuery.must(QueryBuilders.matchQuery("tenantName", courseQuery.getTenantName()));
        }
        //设置组合对象里面的filter对象
        //设置过滤查询的最大价格
        if(null!=courseQuery.getPriceMax()){
            boolQuery.filter(QueryBuilders.rangeQuery("price").lte(courseQuery.getPriceMax()));
        }
        //设置过滤查询结构的最小值
        if(null!=courseQuery.getPriceMin()){
            boolQuery.filter(QueryBuilders.rangeQuery("price").gte(courseQuery.getPriceMin()));
        }
        //设置过滤查询的课程类型
        if(null!=courseQuery.getCourseTypeId()){
            boolQuery.filter(QueryBuilders.termQuery("courseTypeId", courseQuery.getCourseTypeId()));
        }
        //设置filter对象里面的查询条件
        //把组合查询对象设置到主查询对象里面
        Builder.withQuery(boolQuery);
        //设置高亮显示
        HighlightBuilder.Field field = new HighlightBuilder.Field("name").preTags("<font style='color:red'><b>").postTags("</b></font>");
        Builder.withHighlightFields(field);//名字高亮
        //设置聚合查询
        TermsAggregationBuilder tenantNameAggs = AggregationBuilders.terms("tenantNameAggs").field("tenantName");
        TermsAggregationBuilder grandNameAggs = AggregationBuilders.terms("grandNameAggs").field("gradeName");
        Builder.addAggregation(tenantNameAggs);
        Builder.addAggregation(grandNameAggs);
        //查询数据
        //Page<CourseDoc> search = courseRespository.search(Builder.build());
        //加入了高亮显示，上面那一句就不用了，用下面这一个
        AggregatedPage<CourseDoc> page = template.queryForPage(Builder.build(),CourseDoc.class,highlightResultMapper);
        //得到聚合查询的结果集
        //得到机构的结果集
        StringTerms tenantNameAggsList = (StringTerms)page.getAggregation("tenantNameAggs");
        List<AggregationElement> tenantList = new ArrayList<>();
        tenantNameAggsList.getBuckets().forEach(b->{
            AggregationElement aggregationElement = new AggregationElement();
            aggregationElement.setKey(b.getKey().toString());
            aggregationElement.setValue(b.getDocCount());
            tenantList.add(aggregationElement);
        });
        //得到课程的结果集
        List<AggregationElement> grandNameList = new ArrayList<>();
        //得到课程的结果集
        StringTerms GradeNameAggsList = (StringTerms)page.getAggregation("grandNameAggs");
        GradeNameAggsList.getBuckets().forEach(b->{
            AggregationElement aggregationElement = new AggregationElement();
            aggregationElement.setKey(b.getKey().toString());
            aggregationElement.setValue(b.getDocCount());
            grandNameList.add(aggregationElement);
        });
        return new PageList<>(page.getTotalElements(),page.getContent(),tenantList,grandNameList);
    }
    /**
     * 课程的全文检索信息
     * @param courseQuery
     * @return
     */
    public PageList<CourseDoc> CourseSearch1(CourseQuery courseQuery) {
        //判断数据是否为空
        if(courseQuery == null){
            return new PageList<>(0,null);
        }
        //1.创建查询对象
        NativeSearchQueryBuilder Builder = new NativeSearchQueryBuilder();
        //2.分页
        Builder.withPageable(PageRequest.of(courseQuery.getPage()-1, courseQuery.getRows()));
        //3.设置排序方式,默认为降序
        SortOrder desc = SortOrder.DESC;
        if(!StringUtils.isEmpty(courseQuery.getSortType()) && courseQuery.getSortType().toLowerCase().equals("asc")){
            desc = SortOrder.ASC;
        }
        //设置他的排序字段,默认按销售量排序
        String fieldSort = "saleCount";
        if(!StringUtils.isEmpty(courseQuery.getSortField())){
            switch (courseQuery.getSortField().toLowerCase()){
                case "pl":fieldSort = "commentCount";break;
                case "jg":fieldSort = "price";break;
                case "rq":fieldSort = "viewCount";break;
            }
        }
        Builder.withSort(SortBuilders.fieldSort(fieldSort).order(desc));
        //4.设置组合查询条件
        //得到组合查询的查询对象
        BoolQueryBuilder boolQuery = QueryBuilders.boolQuery();
        //设置组合对象里面的must对象
        if(!StringUtils.isEmpty(courseQuery.getKeyword())){
            boolQuery.must(QueryBuilders.matchQuery("name", courseQuery.getKeyword()));
        }
        //设置组合对象里面的filter对象
        //设置过滤查询的最大价格
        if(null!=courseQuery.getPriceMax()){
            boolQuery.filter(QueryBuilders.rangeQuery("price").lte(courseQuery.getPriceMax()));
        }
        //设置过滤查询结构的最小值
        if(null!=courseQuery.getPriceMin()){
            boolQuery.filter(QueryBuilders.rangeQuery("price").gte(courseQuery.getPriceMin()));
        }
        //设置过滤查询的课程类型
        if(null!=courseQuery.getCourseTypeId()){
            boolQuery.filter(QueryBuilders.termQuery("courseTypeId", courseQuery.getCourseTypeId()));
        }
        //设置filter对象里面的查询条件
        //把组合查询对象设置到主查询对象里面
        Builder.withQuery(boolQuery);
        //查询数据
        Page<CourseDoc> search = courseRespository.search(Builder.build());
        return new PageList<>(search.getTotalElements(),search.getContent());
    }
}
