package cn.itsource.hrm.controller;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.domain.Course;
import cn.itsource.hrm.query.CourseQuery;
import cn.itsource.hrm.service.ISearchService;
import cn.itsource.hrm.util.AjaxResult;
import cn.itsource.hrm.util.PageList;
import org.elasticsearch.search.SearchService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class SearchController {
    @Autowired
    private ISearchService searchService;
    @RequestMapping(method = RequestMethod.POST,value = "/online")
    public AjaxResult online(@RequestBody CourseDoc courseDoc){
        searchService.save(courseDoc);
        return AjaxResult.me();
    }

    /**
     * 下线
     * @param courseDoc
     * @return
     */
    @RequestMapping(method = RequestMethod.POST,value = "/offline")
    public AjaxResult offline(@RequestBody CourseDoc courseDoc){
        searchService.delete(courseDoc);
        return AjaxResult.me();
    }
    /**
     * es全文检索
     * @param courseQuery
     * @return
     */
    @RequestMapping(method = RequestMethod.POST,value = "/es/searchCourse")
    public PageList<CourseDoc> searchCourse(@RequestBody CourseQuery courseQuery){
        PageList<CourseDoc> pageList = searchService.CourseSearch(courseQuery);
        return pageList;
    }
}
