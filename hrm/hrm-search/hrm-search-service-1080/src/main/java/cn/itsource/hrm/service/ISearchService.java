package cn.itsource.hrm.service;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.query.CourseQuery;
import cn.itsource.hrm.util.PageList;

public interface ISearchService {
    /**
     * 课程上线方法
     */
    void save(CourseDoc courseDoc);

    /**
     * 课程下线方法
     * @param courseDoc
     */
    void delete(CourseDoc courseDoc);

    /**
     * 课程的全文检索信息
     * @param courseQuery
     * @return
     */
    PageList<CourseDoc> CourseSearch(CourseQuery courseQuery);
}
