package cn.itsource;

import cn.itsource.hrm.SearchApp;
import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.respository.CourseRespository;
import org.elasticsearch.Build;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.sort.FieldSortBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.query.NativeSearchQuery;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SearchApp.class)
public class SearchTest {
    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;
    @Autowired
    private CourseRespository courseRespository;
    /**
     * 创建索引
     */
    @Test
    public void createIndex(){
        //创建索引
        elasticsearchTemplate.createIndex(CourseDoc.class);
        //创建类型
        elasticsearchTemplate.putMapping(CourseDoc.class);
    }
    @Test
    public void addQuery(){

        NativeSearchQueryBuilder Builder = new NativeSearchQueryBuilder();
        //设置组合查询query
        BoolQueryBuilder boolQueryBuilder = QueryBuilders.boolQuery();
        MatchQueryBuilder matchQueryBuilder = QueryBuilders.matchQuery("name", "java");
        RangeQueryBuilder rangeQueryBuilder = QueryBuilders.rangeQuery("saleCount").gt(100).lt(1000);
        //组合查询下设置must查询
        //boolQueryBuilder.must(matchQueryBuilder);
        boolQueryBuilder.must(rangeQueryBuilder);
        boolQueryBuilder.filter(QueryBuilders.termQuery("name", "java基础班"));
        //设置在builder里面
        Builder.withQuery(boolQueryBuilder);
        //设置分页
        Builder.withPageable(PageRequest.of(0, 10));
        //设置排序
        Builder.withSort(new FieldSortBuilder("price").order(SortOrder.DESC));
        NativeSearchQuery build = Builder.build();
        Page<CourseDoc> search = courseRespository.search(build);
        List<CourseDoc> content = search.getContent();
        content.forEach(System.out::println);

    }
}
