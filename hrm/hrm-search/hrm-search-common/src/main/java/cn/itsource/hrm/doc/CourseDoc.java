package cn.itsource.hrm.doc;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Date;

/**
 * 针对于  Employee 表的文档映射
 * indexName：索引库
 * type：类型(表类型)
 */
@Document(indexName = "hrm" , type = "Course")
@Data
public class CourseDoc {

    //对应文档的id  PUT  /index/type/id
    @Id
    private Long id;

    @Field(type = FieldType.Text,analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")    //指定为 不分词，这些注解都是定义映射类型的
    private String name;
    /**
     * 适用人群
     */
    @Field(type = FieldType.Keyword)
    private String forUser;
    @Field(type = FieldType.Long)
    private Long courseTypeId;
    //课程名
    @Field(type = FieldType.Keyword)
    private String gradeName;
    @Field(type = FieldType.Long)
    private Long gradeId;
    @Field(type = FieldType.Long)
    private Long tenantId;
    //机构名
    @Field(type = FieldType.Keyword)
    private String tenantName;
    //销售数
    @Field(type = FieldType.Long)
    private Integer saleCount = 0;
    @Field(type = FieldType.Long)
    private Integer viewCount = 0;
    //评论数
    @Field(type = FieldType.Long)
    private Integer commentCount = 0;
    //是否收费
    @Field(type = FieldType.Integer)
    private Integer charge;
    @Field(type = FieldType.Keyword)
    private String qq;
    //价格
    @Field(type = FieldType.Float)
    private Float price;
    //原价价格
    @Field(type = FieldType.Float)
    private Float priceOld;
    //上线时间
    @Field(type = FieldType.Date)
    private Date online_time;
    //图片
    @Field(type = FieldType.Text)
    private String pic;
}