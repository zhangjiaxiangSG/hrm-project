package cn.itsource.hrm.feign.fallback;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.feign.client.CourseFeign;
import cn.itsource.hrm.util.AjaxResult;
import feign.hystrix.FallbackFactory;
import org.springframework.stereotype.Component;

@Component
public class CourseFallBack implements FallbackFactory<CourseFeign> {
    @Override
    public CourseFeign create(Throwable throwable) {
        return new CourseFeign() {
            @Override
            public AjaxResult online(CourseDoc courseDoc) {
                return AjaxResult.me().setSuccess(false).setMessage("课程上线失败！");
            }

            @Override
            public AjaxResult offline(CourseDoc courseDoc) {
                return AjaxResult.me().setSuccess(false).setMessage("课程下线失败！");
            }
        };
    }
}
