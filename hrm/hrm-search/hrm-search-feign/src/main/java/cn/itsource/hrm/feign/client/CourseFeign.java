package cn.itsource.hrm.feign.client;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.feign.fallback.CourseFallBack;
import cn.itsource.hrm.util.AjaxResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "SEARCH-SERVICE",fallbackFactory = CourseFallBack.class)
public interface CourseFeign {
    @RequestMapping(method = RequestMethod.POST,value = "/online")
    AjaxResult online(@RequestBody CourseDoc courseDoc);
    @RequestMapping(method = RequestMethod.POST,value = "/offline")
    AjaxResult offline(@RequestBody CourseDoc courseDoc);
}
