package cn.itsource.hrm.respository;

import cn.itsource.hrm.doc.CourseDoc;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CourseRespository extends ElasticsearchRepository<CourseDoc,Long> {
}
