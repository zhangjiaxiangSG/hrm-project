package cn.itsource.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

/**
 * 注册中心启动类
 * @EnableEurekaServer : 开启EurekaServer服务端
 */
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
//开启网关注解
@EnableZuulProxy
public class ZuulServerApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(ZuulServerApp.class);
    }
}