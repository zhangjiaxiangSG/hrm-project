package cn.itsource.hrm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * 注册中心启动类
 * @EnableEurekaServer : 开启EurekaServer服务端
 */
//如果导入了mybaties的依赖，就必须写exclude
@SpringBootApplication(exclude= {DataSourceAutoConfiguration.class})
@EnableEurekaClient
//开启配置中心注解配置
@EnableConfigServer
public class ConfigServerApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(ConfigServerApp.class);
    }
}