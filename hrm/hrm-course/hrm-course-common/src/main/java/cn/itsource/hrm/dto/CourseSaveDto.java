package cn.itsource.hrm.dto;

import cn.itsource.hrm.domain.Course;
import cn.itsource.hrm.domain.CourseDetail;
import cn.itsource.hrm.domain.CourseMarket;
import lombok.Data;

import javax.validation.Valid;

/**
 * 存储课程信息的dto
 *  course:{
     courseTypeId:this.addForm.courseTypeId,
     name:this.addForm.name,
     forUser:this.addForm.forUser,
     gradeId:this.addForm.gradeId,
     gradeName:gradeName,
     pic:this.addForm.pic,
     startTime:this.addForm.startTime,
     endTime:this.addForm.endTime
    },
     courseDetail:{
         description:this.addForm.description,
         intro:this.addForm.intro
     },
     courseMarket:{
         charge:this.addForm.chargeId,
         qq:this.addForm.qq,
         price:this.addForm.price,
         priceOld:this.addForm.priceOld,
         expires:this.addForm.expires
     }
 */
@Data
public class CourseSaveDto {
    @Valid
    private Course course;
    @Valid
    private CourseDetail courseDetail;
    @Valid
    private CourseMarket courseMarket;
}
