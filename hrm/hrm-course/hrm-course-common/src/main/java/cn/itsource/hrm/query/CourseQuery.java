package cn.itsource.hrm.query;


import lombok.Data;

/**
 *
 * @author zjx
 * @since 2021-01-14
 */
    @Data
    public class CourseQuery extends BaseQuery{
        private Long courseTypeId;
        private String gradeName;
        private Float priceMin;
        private Float priceMax;
        private String tenantName;
        private String sortField;
        private String sortType;
    }