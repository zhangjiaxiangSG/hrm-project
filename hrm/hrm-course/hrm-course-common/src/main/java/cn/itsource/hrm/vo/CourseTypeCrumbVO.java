package cn.itsource.hrm.vo;

import cn.itsource.hrm.domain.CourseType;
import lombok.Data;

import java.util.List;

/**
 * 查询面包屑的vo
 */
@Data
public class CourseTypeCrumbVO {
    //自己的类型
    private CourseType ownerCourseType;
    //其他同级的类型
    private List<CourseType> otherCourseTypes;
}
