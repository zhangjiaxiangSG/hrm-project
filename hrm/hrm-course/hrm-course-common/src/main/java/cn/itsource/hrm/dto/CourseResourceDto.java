package cn.itsource.hrm.dto;

import cn.itsource.hrm.domain.CourseResource;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

/**
 * 批量添加课程资源
 */
@Data
public class CourseResourceDto {
    private List<CourseResource> list = new ArrayList<>();
    private Long course_id;
}
