package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.CourseType;
import cn.itsource.hrm.mapper.CourseTypeMapper;
import cn.itsource.hrm.service.ICourseTypeService;
import cn.itsource.hrm.util.AssertUtil;
import cn.itsource.hrm.vo.CourseTypeCrumbVO;
import com.baomidou.mybatisplus.mapper.Wrapper;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import com.fasterxml.jackson.annotation.JsonInclude;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * <p>
 * 课程目录 服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
@Service
public class CourseTypeServiceImpl extends ServiceImpl<CourseTypeMapper, CourseType> implements ICourseTypeService {
    @Autowired
    private RedisTemplate<Object,Object> redisTemplate;
    /**
     * 获取课程类型树,采用注解自动缓存的方式
     * @return
     */
    @Override
    @Cacheable(cacheNames = "coursetype",key = "'tree'")
    public List<CourseType> selectCourseTree() {
        return getCourseTypeTrees();
    }

    /**
     * 返回面包屑数据
     * @param courseTypeId
     * @return
     */
    @Override
    @Cacheable(cacheNames ="cousetype",key = "#courseTypeId")
    public List<CourseTypeCrumbVO> findCrumbs(Long courseTypeId) {
        //判断id是否为空
        AssertUtil.isNotNull(courseTypeId, "没有参数可查！");
        //根据类型id查询单个对象
        CourseType courseType = baseMapper.selectById(courseTypeId);
        //判断对象是否存在
        AssertUtil.isNotNull(courseType, "没有次类型的数据");
        //得到当前类型对象的path
        String path = courseType.getPath();
        //封装成数组
        String[] split = path.split("\\.");
        //根据数组批量查询出数据
        List<CourseType> courseTypes = baseMapper.selectBatchIds(Arrays.asList(split));
        //创建一个课程类型集合对象
        List<CourseTypeCrumbVO> CourseTypeCrumbVOs = new ArrayList<>(courseTypes.size());
        //遍历数据
        courseTypes.forEach(c->{
            //1.创建一个面包屑vo对象
            CourseTypeCrumbVO courseTypeCrumbVO = new CourseTypeCrumbVO();
            //2.添加自己
            courseTypeCrumbVO.setOwnerCourseType(c);
            //3.设置别人
            //查询出同类型的数据
            List<CourseType> othersCourseTypes = baseMapper.selectByPid(c.getPid());
            //设置到当前其他同类型的集合里面
            courseTypeCrumbVO.setOtherCourseTypes(othersCourseTypes);
            CourseTypeCrumbVOs.add(courseTypeCrumbVO);
        });
        return CourseTypeCrumbVOs;
    }

    /**
     * 获取课程类型树,采用手动缓存的方式
     * @return
     */
    public List<CourseType> selectCourseTree1() {
        List<CourseType> courseTypeKeys = (List<CourseType>)redisTemplate.opsForValue().get("ss");
        if(courseTypeKeys!=null){
            return courseTypeKeys;
        }
        //如果缓存中没有数据，访问数据库的数据
        List<CourseType> courseTypeTrees = getCourseTypeTrees();
        //设置数据到redis里面
        redisTemplate.opsForValue().set("ss", courseTypeTrees);
        //返回数据
        return courseTypeTrees;
    }

    /**
     * 获取课程类型树
     * @return
     */
    private List<CourseType> getCourseTypeTrees() {
        //获取出所有的课程类型数据
        List<CourseType> courseTypes = baseMapper.selectList(new Wrapper<CourseType>() {
            @Override
            public String getSqlSegment() {
                return null;
            }
        });
        //在数据中得到所有的一级列表的数据
        List<CourseType> firstCourseType = new ArrayList<>();
        courseTypes.forEach(courseType->{
            if(courseType.getPid()==null || courseType.getPid().longValue() == 0){//如果是一级列表，就添加到集合中
                firstCourseType.add(courseType);
            }else{
                for (CourseType type : courseTypes) {
                    if(type.getId().longValue() == courseType.getPid().longValue()){
                        type.getChildren().add(courseType);
                        break;
                    }
                }
            }
        });
        return firstCourseType;
    }
}
