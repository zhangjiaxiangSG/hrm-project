package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.CourseType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 * 课程目录 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface CourseTypeMapper extends BaseMapper<CourseType> {
    /**
     * 根据父id查询出集合
     * @param pid
     * @return
     */
    List<CourseType> selectByPid(Long pid);
}
