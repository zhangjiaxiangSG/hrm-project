package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.CourseMarket;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface CourseMarketMapper extends BaseMapper<CourseMarket> {

}
