package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.CourseResource;
import cn.itsource.hrm.dto.CourseResourceDto;
import cn.itsource.hrm.mapper.CourseResourceMapper;
import cn.itsource.hrm.service.ICourseResourceService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
@Service
public class CourseResourceServiceImpl extends ServiceImpl<CourseResourceMapper, CourseResource> implements ICourseResourceService {

    @Override
    public void patchSave(CourseResourceDto courseResourceDto) {
        //1.获取参数
        List<CourseResource> list = courseResourceDto.getList();
        Long course_id = courseResourceDto.getCourse_id();
        //保存对象
        for (CourseResource courseResource : list) {
            //每个资源设置课程id
            courseResource.setCourseId(course_id);
        }
        //批量保存资源集合
        baseMapper.batchSave(list);
    }
}
