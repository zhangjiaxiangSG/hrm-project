package cn.itsource.hrm.config;

import org.springframework.amqp.core.*;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class RabbitMQConfig {
    //定义邮件的队列名字
    public static final String QUEUE_NAME_EMAIL = "queue_name_email";
    //定义短信的队列名字
    public static final String QUEUE_NAME_SMS = "queue_name_sms";
    //站内信队列
    public static final String QUEUE_NAME_SYSTEM_MESSAGE = "queue_name_system_message";
    //定义交换机
    public static final String EXCHANGE_NAME_DIRECT = "exchange_name_direct";
    //创建交换机
    @Bean
    public Exchange exchangeDirect(){
        return ExchangeBuilder.directExchange(EXCHANGE_NAME_DIRECT).build();
    }
    //创建站内信息队列
    @Bean
    public Queue queueSystemMsg(){
        return QueueBuilder.durable(QUEUE_NAME_SYSTEM_MESSAGE).build();
    }
    //创建邮件队列
    @Bean
    public Queue queueEmail(){
        return QueueBuilder.durable(QUEUE_NAME_EMAIL).build();
    }
    //创建sms队列
    @Bean
    public Queue queueSMS(){
        return QueueBuilder.durable(QUEUE_NAME_SMS).build();
    }
    //绑定站内信息队列到交换机
    @Bean
    public Binding systemBindDirect(){
        return BindingBuilder.bind(queueSystemMsg()).to(exchangeDirect()).with("message.system").noargs();
    }
    //绑定邮件队列信息到交换机
    @Bean
    public Binding EmailBindDirect(){
        return BindingBuilder.bind(queueEmail()).to(exchangeDirect()).with("message.email").noargs();
    }
    //板顶sms队列信息到交换机
    @Bean
    public Binding SMSBindDirect(){
        return BindingBuilder.bind(queueSMS()).to(exchangeDirect()).with("message.sms").noargs();
    }

}
