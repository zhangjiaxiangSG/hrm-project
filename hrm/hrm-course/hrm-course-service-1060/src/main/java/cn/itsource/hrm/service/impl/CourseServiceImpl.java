package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.doc.CourseDoc;
import cn.itsource.hrm.domain.*;
import cn.itsource.hrm.dto.CourseSaveDto;
import cn.itsource.hrm.feign.client.CourseFeign;
import cn.itsource.hrm.mapper.CourseDetailMapper;
import cn.itsource.hrm.mapper.CourseMapper;
import cn.itsource.hrm.mapper.CourseMarketMapper;
import cn.itsource.hrm.mapper.CourseTypeMapper;
import cn.itsource.hrm.service.ICourseService;
import cn.itsource.hrm.util.AjaxResult;
import cn.itsource.hrm.util.AssertUtil;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.elasticsearch.common.Strings;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static cn.itsource.hrm.config.RabbitMQConfig.*;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
@Service
@Transactional
public class CourseServiceImpl extends ServiceImpl<CourseMapper, Course> implements ICourseService {
    @Autowired
    private CourseDetailMapper courseDetailMapper;
    @Autowired
    private CourseMarketMapper courseMarketMapper;
    @Autowired
    private CourseTypeMapper courseTypeMapper;
    @Autowired
    private CourseFeign courseFeign;
    @Autowired
    private RabbitTemplate rabbitTemplate;

    /**
     * 重载方法，添加课程信息
     *
     * @param courseSaveDto
     */
    @Override
    public void save(CourseSaveDto courseSaveDto) {
        //1.获取参数
        Course course = courseSaveDto.getCourse();
        CourseDetail courseDetail = courseSaveDto.getCourseDetail();
        CourseMarket courseMarket = courseSaveDto.getCourseMarket();
        //2.验证
        //3.添加课程信息
        course.setTenantId(26l);
        course.setStatus(0);
        course.setTenantName("源码时代");
        course.setUserId(42l);
        course.setUserName("yhptest");
        baseMapper.insert(course);
        //4.添加课程详情信息
        courseDetail.setId(course.getId());
        courseDetailMapper.insert(courseDetail);
        //5.添加课程——market信息
        courseMarket.setId(course.getId());
        courseMarketMapper.insert(courseMarket);
    }

    /**
     * 上线课程信息
     *
     * @param id
     */
    @Override
    public void onLineCourse(Long id) {
        //查询出当前课程
        Course course = baseMapper.selectById(id);
        //查询课程市场对象
        CourseMarket courseMarket = courseMarketMapper.selectById(course.getId());
        //创建课程索引对象
        CourseDoc courseDoc = new CourseDoc();
        //赋值对象
        courseDoc.setOnline_time(new Date());
        BeanUtils.copyProperties(course, courseDoc);
        BeanUtils.copyProperties(courseMarket, courseDoc);
        //存储到数据库
        course.setStatus(Course.ONLINE_STATUS);
        baseMapper.updateById(course);
        //存储到es中
        AjaxResult online = courseFeign.online(courseDoc);
        AssertUtil.isTrue(online.isSuccess(), "存储到es中失败！");
        //上线后发布信息
        sendMessage(course);
    }

    /**
     * 发送消息
     */
    private void sendMessage(Course course) {
        //发送邮件信息
        List<String> emails = Arrays.asList("19881191949@qq.com","19881191949@qq.com");
        String emailContextTemplate = "尊敬的用户您好，我们新的课程[ %s ]发布啦，快上车，没时间解释了，" +
                "<a href='http://itsource.cn/course/detail/%s'>戳我戳我戳我</a>";
        String emailFormatInfo = String.format(emailContextTemplate,course.getName(),course.getId());
        rabbitTemplate.convertAndSend(EXCHANGE_NAME_DIRECT, "message.email", new SendEmailTo(emails,emailFormatInfo));
        //发送站内信系
        List<Long> ids = Arrays.asList(1l,2l);
        String title = "新课程发布啦！";
        String systemContextTemplate = "尊敬的用户您好，我们新的课程[ %s ]发布啦，快上车，没时间解释了，" +
                "<a href='http://itsource.cn/course/detail/%s'>戳我戳我戳我</a>";
        String SystemFormatInfo = String.format(systemContextTemplate,course.getName(),course.getId());
        rabbitTemplate.convertAndSend(EXCHANGE_NAME_DIRECT, "message.system",new SendSystemTo(ids,title,SystemFormatInfo));
        //发送短信信息
        List<String> phones = Arrays.asList("19881191949","19881191949");
        String smsContextTemplate = "尊敬的用户您好，我们新的课程[ %s ]发布啦，快上车，没时间解释了，" +
                "<a href='http://itsource.cn/course/detail/%s'>戳我戳我戳我</a>";
        String SMSFormatInfo = String.format(smsContextTemplate,course.getName(),course.getId());
        rabbitTemplate.convertAndSend(EXCHANGE_NAME_DIRECT,"message.sms" ,new SendMsgTo(phones,SMSFormatInfo));
    }

    @Override
    public void offLineCourse(Long id) {
        //查询出当前课程
        Course course = baseMapper.selectById(id);
        //查询课程市场对象
        CourseMarket courseMarket = courseMarketMapper.selectById(course.getId());
        //创建课程索引对象
        CourseDoc courseDoc = new CourseDoc();
        //赋值对象
        courseDoc.setOnline_time(new Date());
        BeanUtils.copyProperties(course, courseDoc);
        BeanUtils.copyProperties(courseMarket, courseDoc);
        //存储到数据库
        course.setStatus(Course.OFFLINE_STATUS);
        baseMapper.updateById(course);
        //存储到es中
        AjaxResult offline = courseFeign.offline(courseDoc);
        AssertUtil.isTrue(offline.isSuccess(), "下线到es失败！");
    }
}
