package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.CourseMarket;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface ICourseMarketService extends IService<CourseMarket> {

}
