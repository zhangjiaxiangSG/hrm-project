package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.Course;
import cn.itsource.hrm.dto.CourseSaveDto;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface ICourseService extends IService<Course> {
    /**
     * 重载方法，添加课程信息
     * @param courseSaveDto
     */
    void save(CourseSaveDto courseSaveDto);

    /**
     * 上线课程
     * @param id
     */
    void onLineCourse(Long id);

    /**
     * 下线课程
     * @param id
     */
    void offLineCourse(Long id);
}
