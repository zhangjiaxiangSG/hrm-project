package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.CourseResource;
import cn.itsource.hrm.dto.CourseResourceDto;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface ICourseResourceService extends IService<CourseResource> {
    /**
     * 批量保存指定课程的课程资源
     * @param courseResourceDto
     */
    void patchSave(CourseResourceDto courseResourceDto);
}
