package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.CourseType;
import cn.itsource.hrm.vo.CourseTypeCrumbVO;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 * 课程目录 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface ICourseTypeService extends IService<CourseType> {
    /**
     * 获取课程类型树
     * @return
     */
    List<CourseType> selectCourseTree();

    /**
     * 返回面包屑数据
     * @param courseTypeId
     * @return
     */
    List<CourseTypeCrumbVO> findCrumbs(Long courseTypeId);
}
