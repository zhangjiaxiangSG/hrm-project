package cn.itsource.hrm.controller;

import cn.itsource.hrm.dto.CourseSaveDto;
import cn.itsource.hrm.service.ICourseService;
import cn.itsource.hrm.domain.Course;
import cn.itsource.hrm.query.CourseQuery;
import cn.itsource.hrm.util.AjaxResult;
import cn.itsource.hrm.util.PageList;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/course")
public class CourseController {
    @Autowired
    public ICourseService courseService;

    /**
    * 下线课程
    * @param id  传递的实体
    * @return Ajaxresult转换结果
    */
    @RequestMapping(value="/offLineCourse/{id}",method= RequestMethod.POST)
    public AjaxResult offLineCourse(@PathVariable("id") Long id){
            courseService.offLineCourse(id);
            return AjaxResult.me();
    }
    /**
    * 上线课程
    * @param id  传递的实体
    * @return Ajaxresult转换结果
    */
    @RequestMapping(value="/onLineCourse/{id}",method= RequestMethod.POST)
    public AjaxResult onLineCourse(@PathVariable("id") Long id){
            courseService.onLineCourse(id);
            return AjaxResult.me();
    }
    /**
    * 添加课程信息
    * @param courseSaveDto  传递的实体
    * @return Ajaxresult转换结果
    */
    @RequestMapping(value="/save",method= RequestMethod.POST)
    public AjaxResult save(@Valid @RequestBody CourseSaveDto courseSaveDto){
        try {
            courseService.save(courseSaveDto);
            return AjaxResult.me();
        } catch (Exception e) {
            e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("保存对象失败！"+e.getMessage());
        }
    }

    /**
    * 删除对象信息
    * @param id
    * @return
    */
    @RequestMapping(value="/{id}",method=RequestMethod.DELETE)
    public AjaxResult delete(@PathVariable("id") Long id){
        try {
            courseService.deleteById(id);
            return AjaxResult.me();
        } catch (Exception e) {
        e.printStackTrace();
            return AjaxResult.me().setSuccess(false).setMessage("删除对象失败！"+e.getMessage());
        }
    }

    //获取用户
    @RequestMapping(value = "/{id}",method = RequestMethod.GET)
    public Course get(@PathVariable("id")Long id)
    {
        return courseService.selectById(id);
    }


    /**
    * 查看所有的员工信息
    * @return
    */
    @RequestMapping(value = "/list",method = RequestMethod.GET)
    public List<Course> list(){

        return courseService.selectList(null);
    }


    /**
    * 分页查询数据
    *
    * @param query 查询对象
    * @return PageList 分页对象
    */
    @RequestMapping(value = "/pagelist",method = RequestMethod.POST)
    public PageList<Course> json(@RequestBody CourseQuery query)
    {
        Page<Course> page = new Page<Course>(query.getPage(),query.getRows());
        page = courseService.selectPage(page);
        return new PageList<Course>(page.getTotal(),page.getRecords());
    }
}
