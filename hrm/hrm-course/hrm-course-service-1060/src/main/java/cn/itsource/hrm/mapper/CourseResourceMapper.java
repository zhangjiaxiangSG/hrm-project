package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.CourseResource;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-14
 */
public interface CourseResourceMapper extends BaseMapper<CourseResource> {
    /**
     * 批量保存
     * @param list
     */
    void batchSave(@Param("list") List<CourseResource> list);
}
