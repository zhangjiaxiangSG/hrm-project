package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.Department;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface DepartmentMapper extends BaseMapper<Department> {

}
