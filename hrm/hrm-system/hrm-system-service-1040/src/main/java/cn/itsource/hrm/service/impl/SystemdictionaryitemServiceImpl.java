package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Systemdictionaryitem;
import cn.itsource.hrm.mapper.SystemdictionaryitemMapper;
import cn.itsource.hrm.service.ISystemdictionaryitemService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
@Service
public class SystemdictionaryitemServiceImpl extends ServiceImpl<SystemdictionaryitemMapper, Systemdictionaryitem> implements ISystemdictionaryitemService {
    /**
     * 根据编号查询出数据字典
     * @param sn
     * @return
     */
    @Override
    public List<Systemdictionaryitem> selectBySn(String sn) {
        return baseMapper.selectBySn(sn);
    }
}
