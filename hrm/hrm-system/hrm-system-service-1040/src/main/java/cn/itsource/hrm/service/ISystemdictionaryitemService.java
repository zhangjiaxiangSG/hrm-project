package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
public interface ISystemdictionaryitemService extends IService<Systemdictionaryitem> {
    /**
     * 根据编号查询数据字典
     * @param sn
     * @return
     */
    List<Systemdictionaryitem> selectBySn(String sn);
}
