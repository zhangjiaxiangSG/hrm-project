package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Employee;
import cn.itsource.hrm.domain.Tenant;
import cn.itsource.hrm.dto.TenantRegisterDto;
import cn.itsource.hrm.exception.ErrorCode;
import cn.itsource.hrm.mapper.EmployeeMapper;
import cn.itsource.hrm.mapper.TenantMapper;
import cn.itsource.hrm.service.ITenantService;
import cn.itsource.hrm.util.AssertUtil;
import cn.itsource.hrm.util.GlobalConstant;
import cn.itsource.hrm.util.encrypt.MD5;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
@Service
public class TenantServiceImpl extends ServiceImpl<TenantMapper, Tenant> implements ITenantService {
    @Autowired
    TenantMapper tenantMapper;
    @Autowired
    EmployeeMapper employeeMapper;
    @Override
    public void register(TenantRegisterDto tenantRegisterDto) {
        //1.得到对象
        //得到机构对象
        Tenant tenant = tenantRegisterDto.getTenant();
        //得到人员对象
        Employee employee = tenantRegisterDto.getEmployee();
        //得到资源id
        Long mealId = tenantRegisterDto.getMealId();
        //获取当前时间
        Date now = new Date();
        //检验机构对象的正确性
        CheckTenant(tenant);
        //检验员工对象的正确性
        CheckEmployee(employee);
        //保存机构对象
        saveTenant(tenant,now);
        //保存员工对象
        saveEmployee(employee,now,tenant);
        //设置过期时间
        Date dateExpire = DateUtils.addDays(now, GlobalConstant.Expire_Date);
        //保存到meal表里面
        tenantMapper.insertByParam(tenant.getId(),mealId,dateExpire);
    }
    //保存员工对象
    private void saveEmployee(Employee employee,Date now,Tenant tenant) {
        //设置颜值
        String salt = MD5.getRandomCode(GlobalConstant.SALT_LENGTH);
        employee.setSalt(salt);
        //设置密码
        employee.setPassword(MD5.getMD5(employee.getPassword()+salt));
        //设置输入时间
        employee.setInputTime(now);
        //设置状态
        employee.setState(Employee.STATE_ONLINE);
        //设置部门
        employee.setDeptId(null);
        //设置机构id
        employee.setTenantId(tenant.getId());
        //设置类型
        employee.setType(Employee.COMPANY_MANAGER);
        //保存对象
        employeeMapper.insert(employee);
    }

    //检验员工对象的正确性
    private void CheckEmployee(Employee employee) {
        //检测用户名是否为空
        AssertUtil.isNotNull(employee.getUsername(), ErrorCode.NAME_IS_NULL.getMessage());
        //检测手机号是否正确
        AssertUtil.isTel(employee.getTel(), ErrorCode.PHONE_IS_WRONG.getMessage());
    }

    //检验机构对象的正确性
    private void CheckTenant(Tenant tenant) {
        AssertUtil.isNotNull(tenant.getCompanyNum(), ErrorCode.COMPANY_NUM_NOT_NULL.getMessage());
        AssertUtil.isNotNull(tenant.getTenantTypeId(), ErrorCode.COMPANY_TYPE_NOT_NULL.getMessage());
        //根据公司编号查看是否存在此公司信息
        Tenant tenant1 = tenantMapper.selectByCompanyNum(tenant.getCompanyNum());
        AssertUtil.isNull(tenant1, ErrorCode.COMPANY_IS_EXISTED.getMessage());
    }

    private void saveTenant(Tenant tenant,Date now) {
        //设置机构对象状态的初始值
        tenant.setState(Tenant.NOT_CHECK);
        //设置注册时间
        tenant.setRegisterTime(now);
        //添加机构对象到数据库，返回主键id
        tenantMapper.insert(tenant);
    }
}

