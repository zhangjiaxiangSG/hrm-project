package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.Meal;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface MealMapper extends BaseMapper<Meal> {

}
