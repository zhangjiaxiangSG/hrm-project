package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.Systemdictionaryitem;
import com.baomidou.mybatisplus.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
public interface SystemdictionaryitemMapper extends BaseMapper<Systemdictionaryitem> {
    /**
     * 根据编号查询数据字典
     * @param sn
     * @return
     */
    List<Systemdictionaryitem> selectBySn(String sn);
}
