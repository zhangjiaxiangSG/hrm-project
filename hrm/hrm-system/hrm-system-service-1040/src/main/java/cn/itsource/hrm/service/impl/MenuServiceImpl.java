package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Menu;
import cn.itsource.hrm.mapper.MenuMapper;
import cn.itsource.hrm.service.IMenuService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {

}
