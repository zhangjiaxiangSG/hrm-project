package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Meal;
import cn.itsource.hrm.mapper.MealMapper;
import cn.itsource.hrm.service.IMealService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
@Service
public class MealServiceImpl extends ServiceImpl<MealMapper, Meal> implements IMealService {

}
