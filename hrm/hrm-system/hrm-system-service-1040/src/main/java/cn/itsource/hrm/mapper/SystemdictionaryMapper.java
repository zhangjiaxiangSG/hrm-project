package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.Systemdictionary;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
public interface SystemdictionaryMapper extends BaseMapper<Systemdictionary> {

}
