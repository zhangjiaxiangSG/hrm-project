package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Systemdictionary;
import cn.itsource.hrm.mapper.SystemdictionaryMapper;
import cn.itsource.hrm.service.ISystemdictionaryService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
@Service
public class SystemdictionaryServiceImpl extends ServiceImpl<SystemdictionaryMapper, Systemdictionary> implements ISystemdictionaryService {

}
