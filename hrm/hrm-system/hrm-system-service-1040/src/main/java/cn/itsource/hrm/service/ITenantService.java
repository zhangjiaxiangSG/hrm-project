package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.Tenant;
import cn.itsource.hrm.dto.TenantRegisterDto;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface ITenantService extends IService<Tenant> {
    //机构注册方法
    void register(TenantRegisterDto tenantRegisterDto);
}
