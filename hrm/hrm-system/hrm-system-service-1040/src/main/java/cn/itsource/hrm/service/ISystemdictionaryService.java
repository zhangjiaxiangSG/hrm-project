package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.Systemdictionary;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-10
 */
public interface ISystemdictionaryService extends IService<Systemdictionary> {

}
