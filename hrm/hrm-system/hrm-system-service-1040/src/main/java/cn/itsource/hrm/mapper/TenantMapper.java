package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.Tenant;
import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.Date;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface TenantMapper extends BaseMapper<Tenant> {
    //根据公司编号查询单个数据
    Tenant selectByCompanyNum(String companyNum);
    //添加到机构_套餐中间表
    void insertByParam(@Param("id") Long id, @Param("mealId") Long mealId, @Param("dateExpire") Date dateExpire);
}
