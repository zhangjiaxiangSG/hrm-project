package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.TenantType;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 * 租户(机构)类型表 服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface ITenantTypeService extends IService<TenantType> {

}
