package cn.itsource.hrm.service.impl;

import cn.itsource.hrm.domain.Department;
import cn.itsource.hrm.mapper.DepartmentMapper;
import cn.itsource.hrm.service.IDepartmentService;
import com.baomidou.mybatisplus.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements IDepartmentService {

}
