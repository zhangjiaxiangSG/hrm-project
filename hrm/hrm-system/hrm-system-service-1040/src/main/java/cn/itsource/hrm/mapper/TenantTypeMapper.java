package cn.itsource.hrm.mapper;

import cn.itsource.hrm.domain.TenantType;
import com.baomidou.mybatisplus.mapper.BaseMapper;

/**
 * <p>
 * 租户(机构)类型表 Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface TenantTypeMapper extends BaseMapper<TenantType> {

}
