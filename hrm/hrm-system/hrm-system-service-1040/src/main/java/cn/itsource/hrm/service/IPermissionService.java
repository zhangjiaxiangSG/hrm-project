package cn.itsource.hrm.service;

import cn.itsource.hrm.domain.Permission;
import com.baomidou.mybatisplus.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author zjx
 * @since 2021-01-11
 */
public interface IPermissionService extends IService<Permission> {

}
