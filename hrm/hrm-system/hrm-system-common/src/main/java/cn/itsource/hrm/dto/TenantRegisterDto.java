package cn.itsource.hrm.dto;

import cn.itsource.hrm.domain.Employee;
import cn.itsource.hrm.domain.Tenant;
import lombok.Data;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

/**
 * 用来接受前端传递的参数
 * param = {
 "tenant" : {
 "companyName":this.employee.companyName,
 "companyNum":this.employee.companyNum,
 "address":this.employee.address,
 "logo":this.employee.logo,
 "tenantTypeId":this.employee.tenantTypeId,
 },
 "employee":{
 "username" :this.employee.username,
 "tel" :this.employee.tel,
 "email" :this.employee.email,
 "password" :this.employee.password,
 },
 "mealId":this.employee.mealId
 };
 */
@Data
public class TenantRegisterDto {
    //机构对象
    @Valid
    private Tenant tenant;
    //员工对象
    @Valid
    private Employee employee;
    //所选套餐id
    @NotNull(message = "请选择套餐参！")
    private Long mealId;
}
