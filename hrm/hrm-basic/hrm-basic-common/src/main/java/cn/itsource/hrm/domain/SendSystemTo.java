package cn.itsource.hrm.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendSystemTo {
    //用户
    private List<Long> ids;
    //标题
    private String title;
    //信息
    private String info;
}
