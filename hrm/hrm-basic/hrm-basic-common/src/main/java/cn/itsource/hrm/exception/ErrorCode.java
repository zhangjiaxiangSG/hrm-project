package cn.itsource.hrm.exception;

/**
 * 错我码枚举
 */
public enum ErrorCode {
        //关于公司的
        COMPANY_NUM_NOT_NULL("101","公司编号不正确[%s]"),
        COMPANY_TYPE_NOT_NULL("102","公司类型必选[%s]"),
        COMPANY_IS_EXISTED("103","公司已注册[%s]"),
        //关于用户的
        NAME_IS_NULL("201","用户名为空[%s]"),
        PHONE_IS_WRONG("202","电话号码错误[%s]");
        //错误码
        private String code;
        //错误信息
        private String msg;
        ErrorCode(String code, String msg){
            this.msg = msg;
            this.code = code;
        }
        public String getMessage(){
            //"公司名称不可为空[%s]"  , 100
            return String.format(this.msg, this.code);
        }

}
