package cn.itsource.hrm.util;

import lombok.Data;

@Data
public class AggregationElement {
    private String key;
    private long value;
}
