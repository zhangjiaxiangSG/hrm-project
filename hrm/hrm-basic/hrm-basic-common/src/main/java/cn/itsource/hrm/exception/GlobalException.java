package cn.itsource.hrm.exception;

public class GlobalException extends RuntimeException{
    public GlobalException(String msg){
        super(msg);
    }
}
