package cn.itsource.hrm.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.print.DocFlavor;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class SendMsgTo {
    private List<String> phones;
    private String msg;
}
