package cn.itsource.hrm.util;

/**
 * 全局常量
 */
public class GlobalConstant {
    //验证码的程度
    public static final int SALT_LENGTH = 16;
    //过期时间
    public static final int Expire_Date = 7;
}

