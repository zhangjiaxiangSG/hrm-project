package cn.itsource.hrm.exception;

import cn.itsource.hrm.util.AjaxResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.util.List;

//@RestControllerAdvice：在RestController的基础上做增强 ，这个标签标记的类可以在controller执行前,后做一些事情
//俺们是要在controller执行出异常之后，捕获异常
@RestControllerAdvice
public class GlobalExceptionHandler {

     //在controller执行出异常之后，捕获 Exception 异常
    @ExceptionHandler(value = Exception.class)
    public AjaxResult exceptionHandler(Exception e){
        e.printStackTrace();
        return AjaxResult.me().setSuccess(false).setMessage("啊，服务器故障了，正在暴揍程序员...");
    }
    //参数校验异常 ，通过标签校验
    @ExceptionHandler(value = MethodArgumentNotValidException.class)
    public AjaxResult methodArgumentNotValidExceptionHandler(MethodArgumentNotValidException e){
        e.printStackTrace();
        StringBuilder sb = new StringBuilder();
        List<ObjectError> allErrors = e.getBindingResult().getAllErrors();
        allErrors.forEach(objectError -> {
            sb.append(objectError.getDefaultMessage()).append(" ; ");
        });
        return AjaxResult.me().setSuccess(false).setMessage(sb.toString());
    }
    //我们手动抛的异常
    @ExceptionHandler(value = GlobalException.class)
    public AjaxResult globalExceptionHandler(GlobalException e){
        e.printStackTrace();
        return AjaxResult.me().setSuccess(false).setMessage(e.getMessage());
    }
}
