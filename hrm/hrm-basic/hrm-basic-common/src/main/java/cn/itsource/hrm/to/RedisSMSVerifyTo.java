package cn.itsource.hrm.to;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RedisSMSVerifyTo {
    private String PhoneVerifyCode;
    private Long dateTime;
}
