package cn.itsource.hrm.util;


import cn.itsource.hrm.exception.GlobalException;
import org.springframework.util.StringUtils;

/**
 * 自定义断言工具类
 */
public class AssertUtil {
    //断言对象是否不为空,如果为空就返回异常信息
    public static void isNotNull(Object obj,String errorMsg){
        if(obj instanceof String && StringUtils.isEmpty(obj)){
            throw new GlobalException(errorMsg);
        }
        if(obj == null){
            throw new GlobalException(errorMsg);
        }
    }
    //断言对象为空，如果不为空，返回异常信息
    public static void isNull(Object obj,String errorMsg){
        if(obj instanceof String && !StringUtils.isEmpty(obj)){
            throw new GlobalException(errorMsg);
        }
        if(obj != null){
            throw new GlobalException(errorMsg);
        }
    }
    //断言手机号的正确性，如果不正确，抛出异常
    public static void isTel(String obj,String errorMsg){
        if(!validateDto.isCellPhoneNo(obj)){
            throw new GlobalException(errorMsg);
        }
    }

    public static void isTrue(boolean success, String errorMsg) {
        if(!success){
            throw new GlobalException(errorMsg);
        }
    }
    public static void isTrue(Boolean success, String errorMsg) {
        if(!success){
            throw new GlobalException(errorMsg);
        }
    }

    public static void isEqualsIgnoreCase(String redisImageCodeKey, String imageCode, String s) {
        isNotNull(redisImageCodeKey, "参数不为空！");
        isNotNull(imageCode, "参数不为空！");
        if(!redisImageCodeKey.equalsIgnoreCase(imageCode)){
            throw new GlobalException(s);
        }
    }
}
