package cn.itsource.hrm.consumer;

import cn.itsource.hrm.domain.SendEmailTo;
import cn.itsource.hrm.domain.SendMsgTo;
import cn.itsource.hrm.domain.SendSystemTo;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Channel;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.core.Message;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Component;

import java.io.IOException;


@Component
@Slf4j
public class CourseConsumer {
    //定义邮件的队列名字
    public static final String QUEUE_NAME_EMAIL = "queue_name_email";
    //定义短信的队列名字
    public static final String QUEUE_NAME_SMS = "queue_name_sms";
    //站内信队列
    public static final String QUEUE_NAME_SYSTEM_MESSAGE = "queue_name_system_message";
    //定义交换机
    public static final String EXCHANGE_NAME_DIRECT = "exchange_name_direct";
    @RabbitListener(queues = QUEUE_NAME_EMAIL)
    public void eamilConsumer(@Payload SendEmailTo sendEmailTo, Message message, Channel channel){
        log.info(sendEmailTo.getMsg());
        try {
            //手动签收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RabbitListener(queues = QUEUE_NAME_SMS)
    public void SMSConsumer(@Payload SendMsgTo sendMsgTo, Message message, Channel channel){
        log.info(sendMsgTo.getMsg());
        try {
            //手动签收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @RabbitListener(queues = QUEUE_NAME_SYSTEM_MESSAGE)
    public void systemConsumer(@Payload SendSystemTo sendSystemTo, Message message, Channel channel){
        log.info(sendSystemTo.getInfo());
        log.info(sendSystemTo.getTitle());
        try {
            //手动签收
            channel.basicAck(message.getMessageProperties().getDeliveryTag(),true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
